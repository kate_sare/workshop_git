**Prerequisitos**

Instalar [Gitlens para VSCode](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
---

# Workshop GIT - Casos prácticos

A continuación emularemos una sesión de trabajo de un día cualquiera, mencionando soluciones a situaciones comunes con manejo de ramas:

---

## 1.- Manejar ramas (branches)

### 1.1.- Quiero crear una rama que nace de otra

```
>$ git checkout -b rama_hija rama_madre
```
Ejemplo:

```
>$ git checkout -b crear_banner release1
```

### 1.1.2.-  Crear desde Gitlab


### 1.2.- Quiero renombrar un branch
```
>$ git branch -m nombre_viejo nombre_nuevo
```

Ejemplo:
```
>$ git branch crear_banner R1_CRED-1234_crear_banner

```

### 1.3.- ¿Cómo borro ramas: locales y en remote?

#### - Para borrarla de nuestro working directory:

Debemos hacer checkout a una rama distinta de la que queremos borrar, para poder efectuar este comando:

```
>$ git checkout nombre_rama_distinta_a_la_que_quiero_borrar
```

```
>$ git branch -d nombre_rama
```

Ejemplo:
```
>$ git checkout release1
```

```
>$ git branch -d R1_CRED-1234_crear_banner
```

Si quieres borrar una rama a pesar de no haber mergeado los cambios a otra rama, puedes forzar el borrado con el siguiente comando:

```
>$ git branch -D nombre_rama
```

#### - Para borrarla de remote:

```
>$ git push origin --delete nombre_rama
```

Ejemplo:
```
>$ git push origin --delete  R1_CRED-1234_crear_banner
```

### 1.4.- Actualizar una rama
Un caso particular: un miembro de mi equipo actualizó la rama release1 y para traerme esos cambios a la rama hija en la que estoy trabajando, debo combinar esos cambios a los de mi rama para evitar mantenerme desactualizado.

### 1.4.1- git fetch vs git pull

diferencia entre fetch y pull

consultas los cambios que hay en el repositorio con respecto a tu copia local
```
>$ git fetch
```
bajas los cambios a tu local
```
>$ git pull origin release1
```

### 1.4.1- git merge vs git rebase

Git Merge
![git-merge](https://developer.atlassian.com/blog/2014/12/pull-request-merge-strategies-the-great-debate/what-is-a-merge.gif)

Git Rebase
![git-rebase](https://developer.atlassian.com/blog/2014/12/pull-request-merge-strategies-the-great-debate/what-is-a-rebase.gif)
```
>$ git checkout -b merge
>$ vim nuevoArchivo.txt
>$ git add .
>$ git commit -m "feat: nuevo archivo"
```

// subo un cambio a release 1

```
>$ git checkout release1
>$ git pull origin release1
>$ git checkout merge
```

```
>$ git merge release1
>$ git log --graph
```

```
>$ git rebase release1
>$ git log --graph
```

### 1.5 Ordenar las ramas por la última a la que se le hizo commit:
```
>$ git for-each-ref --sort=-committerdate refs/heads/
```

---

## 2.- Ver estado de mi repositorio

### 2.1.- Quiero listar todas mis ramas

#### -  En working directory
```
>$ git branch

```

Si además, quiero filtrar por ciertos patrones del nombre de la rama, podemos usar wildcards:
```
>$ git branch --list R1_*

```
#### - En remote y working directory

```
>$ git branch --all

```

### 2.2.- Quiero ver estado de mis commits

Para ver el historial de mis commits, puedo hacer lo siguiente:
```
>$ git log

```

Si quiero sintetizar la información usamos el flag --oneline:
```
>$ git log --oneline

```
Y si deseo ver cómo se bifurcan mis cambios, podemos visualizar un grafo con el flag --graph:
```
>$ git log --graph

```
//TODO: Explicar los índices de cada item:
Además de este historial, existe uno más detallado que indica todos los cambios realizados desde que se clona el repositorio:
```
>$ git reflog

```
---
## 3.- Actualizar cambios

![stash](https://cdn-images-1.medium.com/max/1200/1*-s5rmyj_-nckv9YcdkUNZQ.png)

### 3.1.- Almacenar localmente cambios sin llevarlos al stage area, evita perder los cambios realizados cuando debemos enfocarnos en otra tarea diferente a partir de un estado limpio.
Nos posicionamos en la rama funcionalidad_a, la cual continene un archivo llamado hero.service.ts

```
>$ git checkout funcionalidad_a

```

Nos piden atender el Ticket-ABC: "Centralizar manejo de error en servicios de la app."

```
 private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }

```
Opcional: 

Sustituimos 

```
.pipe(catchError(new Error('An error has occurred')));

```
por

```
.pipe(catchError(this.handleError));

```

En medio de la implementación de nuestro cambio, nos piden atender el Ticket-XYZ: "Solucionar bug en bff", para evitar perder los cambios realizados al cambiar el working directory dentro del
mismo proyecto, procedemos a guardarlos localmente y luego a guardarlos usando el **stash** ejecutando los siguientes comandos:

```
git stash save 'error_handler'

```

Listamos las entradas del stash

```
git stash list

```
Una vez guardado en el stash, podemos volver a otro estado del directorio de trabajo sin perder los cambios.

**Demo - git stash save**: 

![git stash save](https://giant.gfycat.com/FirstAcademicElephantseal.gif)

### 3.2.- Aplicar cambios desde el stash

Si vamos a retomar el trabajo de los cambios almacenados en el stash, usamos el siguiente comando

Opcional

```
git stash list

```

```
git stash apply stash@{i} // i es el indice del cambio en el stash

```
(Los cambios se aplican pero la entrada se mantiene en el stash)

**Demo - git stash apply**: 

![git stash apply](https://giant.gfycat.com/DangerousEducatedDinosaur.gif)

### 3.3.- Eliminar entrada en el stash

Opcional

```
git stash list

```

```
git stash drop stash@{0} // i es el indice del cambio en el stash
```

#### Cherry-pick:

![cherry-pick](https://sogilis.com/wp-content/uploads/2015/05/cherrypick.gif)

Permite insertar cambios individualmente en otras ramas, suele ser util en ramas 
que contienen hot-fixes hechos en master que luego deben replicados en otras ramas
de desarrollo o QA. 

Actualizamos nuestsra version y nos situamos en la rama **funcionalidad_a**
```
git pull

```
```
git checkout funcionalidad_a

```

Por ejemplo, agregamos un cambio que centraliza el manejo de errores en la rama 
**funcionalidad_a** y queremos que la rama **funcionalidad_b** tambien tenga esa
funcionalidad, entonces reutilizamos el cambio mezclado en **funcionalidad_a**

Llevamos el cambio a compartir al stage area haciendo un commit local

Obtenemos el commit hash del commit pasado para insertarlo en una rama destino
```
git log

```
Copiamos el hash y nos movemos a la rama donde aplicaremos el cambio 
```
git checkout funcionalidad_b

```
Aplicamos el Cherry-Pick
```
git cherry-pick <commit_hash>

```

**Demo**: 

![git stash apply](https://giant.gfycat.com/DecisiveMajorImperatorangel.gif)


Observaciones:

*  Si se desea mantener una rama funcionalidad actualizada con otra rama, por ejemplo:
   funcionalidad_a con Desarrollo-CI entonces es recomendable ejecutar MERGE.

*  Si se desea reutilizar un cambio desde una rama excluyendo otros cambios, 
   entonces es recomendable ejecutar CHERRY-PICK

## 4.- Desechar cambios

### 4.1.- Me falto agregar algo más a mi último commit que no he pusheado

####- Si quiero agregar más cambios a mi último commit:

Agrega los cambios que te hayan faltado con:
```
>$ git add nombre_file_a_agregar
```

Seguidamente, realiza el commit a tu cambios con el siguiente comando:
```
>$ git commit --amend
```

Ahora se te abrirá un editor de texto, en el que puedes agregar un nuevo texto o persistir el que ya existe en el commit.

#### - Por otro lado, si sólo quiero editar el texto del commit, después de hacer el commit:

```
>$ git commit --amend
```

### 4.2.- Hice commit de mis cambios en una rama que no era, cómo hago para cambiarlos

#### - Caso reset:

Deshacemos el último commit realizado en la rama en la que me encuentro, es decir, regresamos los cambios del staging area al working directory:

```
>$ git reset HEAD~ --soft
```
Guardamos temporalmente esos cambios:
```
>$ git stash
```
Nos movemos a la rama en la que queremos guardar efectivamente los cambios:
```
>$ git checkout nombre-del-branch-correcto
```
Restauramos los cambios de la pila de stash:
```
>$ git stash pop
```

Volvemos a agregar todos los cambios necesarios en la rama correcta:
```
>$ git add .
>$ git commit -m "your message here"
```
Ahora si los cambios están en la rama correcta.



### 4.3.- Deshacer cambios en working directory:

#### - He realizado cambios, pero aún no he agregado ninguno al stage area con git add:
```
>$ git checkout -- .
```

ó
```
>$ git checkout nombre_file
```

### 4.4.- Deshacer cambios en staging area:

#### - He editado cambios sobre archivos que ya están en el stage(fueron agregados con add), pero quiero deshacerlos, es decir quiero regresarlos al working directory(Sólo add):
```
>$ git reset HEAD
```

ó
```
>$ git reset HEAD nombre_file
```

En nuestro caso particular, sólo podemos realizar esto en ramas distintas de Desarrollo-CI, dado que no tenemos permisos sobre esa rama.

#### - Realicé commits de cambios que quiero deshacer y regresarlos al staging area, pero quiero conservar en mi working directory esos cambios (Commit conservando cambios):

Este comando, revertirá el número de commits indicado. Si cambiamos el 1 por el 2, nos regresará hacia atrás 2 commits, pero nos mantendrá en el staging area los cambios realizados:

```
>$ git reset --soft HEAD~1
```


#### - Realicé commits de cambios que quiero deshacer y quiero borrarlos inclusive del working directory(Commit sin conservar cambios):

Este regresará atrás un commit y borrará del working directory los cambios asociados:
```
$ >$ git reset --hard HEAD~1
```

Si listamos con git reflog, los commits realizados, podremos ubicar el que queremos borrar, ya sea a través de su index o su sha:
```
>$ git reset --hard HEAD@{6}
```
ó
```
>$ git reset --hard commit_sha1
```

### 4.5.- Deshacer cambios en remote:
#### - Ya el commit lo subí a remote con un push, pero quiero borrarlos:
Este comando me devolverá al último commit hecho y pusheado:
```
git reset HEAD^ --hard
```

ó indico el commit desde el que quiero mantener mis cambios
```
git reset HEAD@{10} --hard
```

Y después hacemos un push forzado sobre esa rama
```
git push -f
```

---







