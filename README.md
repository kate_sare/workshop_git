**Prerequisitos**

1.- Clonar repositorio:

```
git clone https://gitlab.com/kate_sare/workshop_git.git
```
Las credenciales para ese proyecto son:

username: bech
contraseña: bech2019

2.- Instalar [Gitlens para VSCode](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)


---

# Workshop GIT - Casos prácticos

A continuación emularemos una sesión de trabajo de un día cualquiera, mencionando soluciones a situaciones comunes con manejo de ramas:

---

## 1.- Manejar ramas (branches)

### 1.1.- Quiero crear una rama que nace de otra

```
>$ git checkout -b rama_hija rama_madre
```
/* TODO(Franco): Crear desde Gitlab también */

Ejemplo:
```
>$ git checkout -b Desarrollo master
```

### 1.2.- Quiero renombrar un branch
```
>$ git checkout -m nombre_viejo nombre_nuevo
```
// TODO(Franco): Aqui crear todas las ramas haciendo alusión al nuevo estandar. 

Ejemplo:
```
>$ git checkout -m Desarrollo Desarrollo-CI
```

### 1.3.- ¿Cómo borro ramas: locales y en remote?
Para este caso, creemos una rama nueva y la llamamos cambios:
```
>$ git checkout -b cambios
```

Subimos algunos cambios y hacemos push de los mismos. Posteriormente, procedemos a borrar la rama:

#### - Para borrarla de nuestro working directory:

Debemos hacer checkout a otra rama, para poder efectuar este comando:

```
>$ git branch -d nombre_rama
```
Ejemplo:
```
>$ git branch -d cambios
```

#### - Para borrarla de remote:

```
>$ git push origin --delete nombre_rama
```

Ejemplo:
```
>$ git push origin --delete cambios
```

### 1.4.- Actualizar una rama
/* TODO(Franco): Completar el flujo de este caso particular */
Un caso particular: un miembro de mi equipo actualizó la rama release1 y para traerme esos cambios a la rama hija en la que estoy trabajando, debo combinar esos cambios a los de mi rama para evitar mantenerme desactualizado.

//TODO(Franco): Explicar como actualizar la rama madre y luego hacer los comandos de la mezcla:


// TODO(Franco): Explicar diferencia entre fetch y pull
```
>$ git fetch origin

```

// TODO(Franco): Explicar cómo realizar el rebase y merge
git merge ó git rebase


---

## 2.- Ver estado de mi repositorio

### 2.1.- Quiero listar todas mis ramas

#### -  En working directory
```
>$ git branch

```

Si además, quiero filtrar por ciertos patrones del nombre de la rama, podemos usar wildcards:
```
>$ git branch --list release1/*

```
#### - En remote y working directory

```
>$ git branch --all

```

### 2.2.- Quiero ver estado de mis commits

Para ver el historial de mis commits, puedo hacer lo siguiente:
```
>$ git log

```

Si quiero sintetizar la información usamos el flag --oneline:
```
>$ git log --oneline

```
Y si deseo ver cómo se bifurcan mis cambios, podemos visualizar un grafo con el flag --graph:
```
>$ git log --graph

```
//TODO: Explicar los índices de cada item:
Además de este historial, existe uno más detallado que indica todos los cambios realizados desde que se clona el repositorio:
```
>$ git reflog

```
---
## 3.- Actualizar cambios
// TODO(Rafa): Explicar cómo hacerlo con gitlens tb
### 3.1.- Hay cambios nuevos en una rama distinta a la que estoy o en la que estoy parado, pero no he terminado de hacer mis cambios en mi rama actual, cómo actualizo sin perder mis cambios sin necesidad de hacer un commit.

```
>$ git stash

```

### 3.2.- Hice commit de mis cambios en una rama que no era, cómo hago para cambiarlos

#### - Caso reset:

Deshacemos el último commit realizado en la rama en la que me encuentro, es decir, regresamos los cambios del staging area al working directory:

```
>$ git reset HEAD~ --soft
```
Guardamos temporalmente esos cambios:
```
>$ git stash
```
Nos movemos a la rama en la que queremos guardar efectivamente los cambios:
```
>$ git checkout nombre-del-branch-correcto
```
Restauramos los cambios de la pila de stash:
```
>$ git stash pop
```

Volvemos a agregar todos los cambios necesarios en la rama correcta:
```
>$ git add .
>$ git commit -m "your message here"
```
Ahora si los cambios están en la rama correcta.


#### - Caso cherry-pick:

También podemos usar cherry-pick para esta situación:
```
>$ git checkout nombre-del-branch-correcto

```

Traemos el último cambio de la rama en la que quedó nuestro commit:
```
>$ git cherry-pick master
```

Sin embargo, mediante este cambio, no se borra el commit en la rama de dónde se extrajo, por tanto debemos borrarlo manual:

>$ git checkout master
>$ git reset HEAD~ --hard

// TODO(Rafa): Explicar cómo realizar cherry pick
### 3.3.- Elegir que commit en específico quiero mergear a otra rama
cherry-pick


### 3.4.- Me falto agregar algo más a mi último commit que no he pusheado

####- Si quiero agregar más cambios a mi último commit:

Agrega los cambios que te hayan faltado con:
```
>$ git add nombre_file_a_agregar
```

Seguidamente, realiza el commit a tu cambios con el siguiente comando:
```
>$ git commit --amend
```

Ahora se te abrirá un editor de texto, en el que puedes agregar un nuevo texto o persistir el que ya existe en el commit.

#### - Por otro lado, si sólo quiero editar el texto del commit, después de hacer el commit:

```
>$ git commit --amend
```
---

## 4.- Desechar cambios

### 4.1.- En working directory:

#### - He realizado cambios, pero aún no he agregado ninguno al stage area con >$ git add:
```
>$ git checkout -- .
```

ó
```
>$ git checkout nombre_file
```

#### - He editado cambios sobre archivos que ya están en el stage(fueron agregados con add), pero quiero deshacerlos, es decir quiero regresarlos al working directory:
```
>$ git reset HEAD
```

ó
```
>$ git reset HEAD nombre_file
```

//TODO: Explicar que solo en ramas distintas de Desarrollo-CI se puede hacer esto, dado que no tenemos permisos sobre esa rama.

#### - Realicé commits de cambios que quiero deshacer y regresarlos al staging area, pero quiero conservar en mi working directory esos cambios:

Este comando, revertirá el número de commits indicado. Si cambiamos el 1 por el 2, nos regresará hacia atrás 2 commits, pero nos mantendrá en el staging area los cambios realizados:

```
$ >$ git reset --soft HEAD~1
```


#### - Realicé commits de cambios que quiero deshacer y quiero borrarlos inclusive del working directory:

Este regresará atrás un commit y borrará del working directory los cambios asociados:
```
$ >$ git reset --hard HEAD~1
```

Si listamos con git reflog, los commits realizados, podremos ubicar el que queremos borrar, ya sea a través de su index o su sha:
```
>$ git reset --hard HEAD@{6}
```
ó
```
>$ git reset --hard commit_sha1
```

#### - Ya el commit lo subí a remote con un push, pero quiero borrarlos:
Este comando me devolverá al último commit hecho y pusheado:
```
git reset HEAD^ --hard
```

ó indico el commit desde el que quiero mantener mis cambios
```
git reset HEAD@{10} --hard
```

Y después hacemos un push forzado sobre esa rama
```
git push -f
```

---
